# trivia-vue
This is a single page triva-quiz app.

## INSTRUCTIONS FOR PROJECT SETUP
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### COMPONENTS

### GameMenu
The start page where the user can enter an in-game name and a “Start Game” button.

### GamePlay
A smart component that handles the active game. It displays the QuizQuestion component 
and it contains the question data. It listens for the answers from QuizQuestion and 
emits the full question and answer dataset to to the MainApp component. 

### GameOver
A component that displays the result of the current game. It displays the QuizREsult component.
Include buttons with options to restart the game or go back to the GameMenu component.

### QuizQuestion
A simple component that displays the question with the available options as clickable buttons. 
The component receives the current question as a prop. 
The component emit a custom event when the answer is selected.

### QuizResults
The component receives a data set to display the result from an answered question.