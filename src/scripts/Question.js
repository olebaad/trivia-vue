export default class Question {
  constructor(questionObject) {
    this.category = questionObject.category;
    this.type = questionObject.type;
    this.difficulty = questionObject.difficulty;
    this.question = questionObject.question;
    this.correctAnswer = questionObject.correct_answer;
    this.incorrectAnswers = questionObject.incorrect_answers;
    //Creating an array with suffled order of alternatives
    this.alternativesArray = this.createAlterntives(this.correctAnswer, this.incorrectAnswers);
  }
  createAlterntives(correctAnswer, incorrectAnswers) {
    const fullArray = incorrectAnswers.slice();
    fullArray.push(correctAnswer);

    return this.shuffleArray(fullArray);
  }
  // Method used to shuffle alternatives
  shuffleArray(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there still are elements to shuffle
    while (0 !== currentIndex) {
      // pick a remaining element
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // Swap elements:
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
}