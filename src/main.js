import Vue from 'vue';
import VueRouter from 'vue-router';

import MainApp from './App.vue';
import GameMenu from './components/GameMenu.vue';
import QuizQuestion from './components/QuizQuestion.vue';
import GamePlay from './containers/GamePlay.vue';
import GameOver from './components/GameOver.vue';
import QuizResult from './components/QuizResult.vue';

//import bootstrapVue plugins
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

// import the bootstrap dependencies
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Use BootstrapVue
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

//Use VueRouter 
Vue.use(VueRouter);

// Using axios for dataloading
const routes = [
  { path: '/', component: GameMenu },
  { path: '/QuizQuestion', component: QuizQuestion },
  { path: '/GamePlay', component: GamePlay },
  { path: '/GameOver', component: GameOver },
  { path: '/QuizResult', component: QuizResult }
];

const router = new VueRouter({ 
  routes 
});

new Vue({
  el: '#app',
  router,
  // Render frontpage
  render: h => h(MainApp)
});

